﻿using System.Collections.Generic;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Services.Interfaces
{
    public interface ICourseService
    {
        CourseResponseDto Create(CourseRequestDto courseRequestDto);

        Course GetCourse(int id);

        CourseResponseDto GetById(int id);

        CouresViewModel GetViewModelById(int id);

        IEnumerable<CourseResponseDto> Get(CourseQueryParameters filterParameters);

        List<Course> GetAllCourses();

        CourseResponseDto Update(int id, CourseRequestDto courseRequestDto);

        void Delete(int id);

        public List<CourseType> Get();

        void Enroll(int courseId, IList<User> students);

        void Unenroll(int courseId, IList<User> students);

        List<User> GetRegisteredStudents(int courseId);

        List<User> GetUnregisteredStudents(int courseId);
    }
}
