﻿using System.Collections.Generic;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Identity.Authentication;
using LearningPlatform.Models;

namespace LearningPlatform.Services.Interfaces
{
    public interface IUserService
    {
        UserResponseDto Create(UserRequestDto userDto);

        UserResponseDto GetById(int id);

        UpdateStudentViewModel GetRequestDto(int id);

        bool UserExists(string email);

        bool ValidateUser(UserCred userCred);

        User GetUserByEmail(string email);

        List<User> GetAllStudents();

        IEnumerable<UserResponseDto> Get(UserQueryParameters filterParameters);

        UserRequestDto Update(int id, UserRequestDto dto);

        User Delete(int id);

        void Upload(UpdateStudentViewModel viewModel, User user);
    }
}