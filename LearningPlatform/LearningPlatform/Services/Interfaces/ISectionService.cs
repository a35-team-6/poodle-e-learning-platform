﻿using System.Collections.Generic;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;

namespace LearningPlatform.Services.Interfaces
{
    public interface ISectionService
    {
        SectionResponseDto Create(SectionRequestDto sectionDto);

        SectionResponseDto GetById(int id);

        SectionRequestDto GetRequestDto(int id);

        Section GetSectionById(int id);

        List<SectionResponseDto> Get(SectionQueryParameters filterParameters);

        SectionResponseDto Update(int id, SectionRequestDto sectionDto);

        void Delete(Section section);
    }
}
