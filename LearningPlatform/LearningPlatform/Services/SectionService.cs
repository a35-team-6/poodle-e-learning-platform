﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;
using LearningPlatform.Repository.Interfaces;
using LearningPlatform.Services.Interfaces;

namespace LearningPlatform.Services
{
    public class SectionService : ISectionService
    {
        private readonly ISectionRepository sectionRepository;
        private readonly ICourseService courseService;
        private readonly IMapper mapper;

        public SectionService(ISectionRepository sectionRepository, ICourseService courseService, IMapper mapper)
        {
            this.sectionRepository = sectionRepository;
            this.courseService = courseService;
            this.mapper = mapper;
        }

        public SectionResponseDto Create(SectionRequestDto sectionDto)
        {
            var section = this.mapper.Map<Section>(sectionDto);

            var course = this.courseService.GetCourse(section.CourseId);

            if (sectionDto.Order > course.Sections.Count + 1)
            {
                section.Order = course.Sections.Count + 1;
            }
            else
            {
                UpdateSectionCreateOrder(sectionDto);
            }

            this.sectionRepository.Create(section);

            return this.mapper.Map<SectionResponseDto>(section);
        }

        public SectionResponseDto GetById(int id)
        {
            var section = this.sectionRepository.GetById(id);
            return this.mapper.Map<SectionResponseDto>(section);
        }

        public SectionRequestDto GetRequestDto(int id)
        {
            var section = this.sectionRepository.GetById(id);
            return this.mapper.Map<SectionRequestDto>(section);
        }

        public Section GetSectionById(int id)
        {
            return this.sectionRepository.GetById(id);
        }

        public List<SectionResponseDto> Get(SectionQueryParameters quryParameters)
        {
            var sections = this.sectionRepository.Get(quryParameters);
            var dtos = new List<SectionResponseDto>();
            foreach (var section in sections)
            {
                dtos.Add(mapper.Map<SectionResponseDto>(section));
            }

            return dtos;
        }

        public SectionResponseDto Update(int id, SectionRequestDto sectionDto)
        {
            var section = GetSectionById(id);

            if (!(section.Order == sectionDto.Order))
            {
                UpdateSectionsOrder(id, sectionDto);
            }

            var updatedSection = this.sectionRepository.Update(id, sectionDto);

            return mapper.Map<SectionResponseDto>(updatedSection);
        }

        public void Delete(Section section)
        {
            this.UpdateSectionsDeleteOrder(section);
            this.sectionRepository.Delete(section.Id);
        }

        private void UpdateSectionCreateOrder(SectionRequestDto sectionDto)
        {
            var order = sectionDto.Order;
            var course = courseService.GetCourse(sectionDto.CourseId);
            var sections = course.Sections;

            var counter = 0;
            foreach (var item in sections.OrderBy(s => s.Order))
            {
                counter++;

                if (item.Order == order)
                {
                    counter++;
                }

                if (item.Order == counter)
                {
                    continue;
                }

                item.Order = counter;

                var dto = this.mapper.Map<SectionRequestDto>(item);
                var updateSection = this.sectionRepository.Update(item.Id, dto);
            }
        }

        private void UpdateSectionsOrder(int id, SectionRequestDto sectionDto)
        {
            var newOrder = sectionDto.Order;
            var course = this.courseService.GetCourse(sectionDto.CourseId);
            var section = this.GetSectionById(id);
            var oldOrder = section.Order;
            var sections = course.Sections;
            var counter = 0;
            var hasBeenPassed = false;

            foreach (var item in sections.OrderBy(s => s.Order))
            {
                if (item.Id == section.Id)
                {
                    continue;
                }

                counter++;

                if (newOrder < oldOrder)
                {
                    if (counter == oldOrder)
                    {
                        hasBeenPassed = true;
                    }

                    if (item.Order == newOrder && hasBeenPassed == false)
                    {
                        counter++;
                    }
                }
                else
                {
                    if (counter == oldOrder)
                    {
                        hasBeenPassed = true;
                    }

                    if (counter == newOrder && hasBeenPassed == true)
                    {
                        counter++;
                    }
                }

                if (item.Order == counter)
                {
                    continue;
                }

                item.Order = counter;
                var dto = this.mapper.Map<SectionRequestDto>(item);
                var updateSection = this.sectionRepository.Update(item.Id, dto);
            }
        }

        private void UpdateSectionsDeleteOrder(Section section)
        {
            var order = section.Order;
            var course = this.courseService.GetCourse(section.CourseId);
            var sections = course.Sections;

            foreach (var item in sections.OrderBy(s => s.Order))
            {
                if (item.Order < order)
                {
                    continue;
                }

                item.Order--;

                var dto = this.mapper.Map<SectionRequestDto>(item);
                var updateSection = this.sectionRepository.Update(item.Id, dto);
            }
        }
    }
}
