﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;
using LearningPlatform.Models.Enums;
using LearningPlatform.Repository.Interfaces;
using LearningPlatform.Services.Interfaces;

namespace LearningPlatform.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository coursesRepository;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public CourseService(ICourseRepository coursesRepository, IUserService userService, IMapper mapper)
        {
            this.coursesRepository = coursesRepository;
            this.userService = userService;
            this.mapper = mapper;
        }

        public CourseResponseDto Create(CourseRequestDto courseRequestDto)
        {
            var course = mapper.Map<Course>(courseRequestDto);
            var createCourse = this.coursesRepository.Create(course);
            var createdCourse = mapper.Map<CourseResponseDto>(createCourse);

            return createdCourse;
        }

        public CourseResponseDto GetById(int id)
        {
            var course = this.coursesRepository.GetById(id);
            var responseCourse = mapper.Map<CourseResponseDto>(course);

            return responseCourse;
        }

        public Course GetCourse(int id)
        {
            return this.coursesRepository.GetById(id);
        }

        public CouresViewModel GetViewModelById(int id)
        {
            var responseCourse = GetById(id);
            var viewModel = mapper.Map<CouresViewModel>(responseCourse);

            return viewModel;
        }

        public IEnumerable<CourseResponseDto> Get(CourseQueryParameters queryParameters)
        {
            var courses = this.coursesRepository.Get(queryParameters);

            var responseCourses = new List<CourseResponseDto>();

            foreach (var course in courses)
            {
                responseCourses.Add(mapper.Map<CourseResponseDto>(course));
            }

            return responseCourses;
        }

        public CourseResponseDto Update(int id, CourseRequestDto courseRequestDto)
        {
            var updateCourse = this.coursesRepository.Update(id, courseRequestDto);
            var updatedCourse = this.mapper.Map<CourseResponseDto>(updateCourse);

            return updatedCourse;
        }

        public void Delete(int id)
        {
            this.coursesRepository.Delete(id);
        }

        public List<CourseType> Get()
        {
            var courseTypes = new List<CourseType>();

            foreach (var type in Enum.GetValues(typeof(CourseType)))
            {
                courseTypes.Add((CourseType)type);
            }

            return courseTypes;
        }

        public void Enroll(int courseId, IList<User> students)
        {
            this.coursesRepository.Enroll(courseId, students);
        }

        public void Unenroll(int courseId, IList<User> students)
        {
            this.coursesRepository.Unenroll(courseId, students);
        }

        public List<Course> GetAllCourses()
        {
            return this.coursesRepository.GetAllCourses();
        }

        public List<User> GetRegisteredStudents(int courseId)
        {
            return this.userService.GetAllStudents()
                                   .Where(u => u.Courses.Contains(GetCourse(courseId)))
                                   .ToList();
        }

        public List<User> GetUnregisteredStudents(int courseId)
        {
            return this.userService.GetAllStudents()
                                   .Where(u => !(u.Courses.Contains(GetCourse(courseId))))
                                   .ToList();
        }
    }
}
