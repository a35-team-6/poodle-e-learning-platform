﻿using System.Collections.Generic;
using System.IO;

using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Identity.Authentication;
using LearningPlatform.Models;
using LearningPlatform.Repository.Interfaces;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Hosting;

namespace LearningPlatform.Repository
{
    public class UserService : IUserService
    {
        private readonly IUserRepository usersRepository;
        private readonly IMapper mapper;
        private readonly IWebHostEnvironment hostEnvironment;

        public UserService(IUserRepository usersRepository, IMapper mapper, IWebHostEnvironment hostEnvironment)
        {
            this.usersRepository = usersRepository;
            this.mapper = mapper;
            this.hostEnvironment = hostEnvironment;
        }

        public UserResponseDto Create(UserRequestDto userDto)
        {
            var user = mapper.Map<User>(userDto);
            usersRepository.Create(user);
            return mapper.Map<UserResponseDto>(user);
        }

        public UserResponseDto GetById(int id)
        {
            var user = usersRepository.GetById(id);
            return this.mapper.Map<UserResponseDto>(user);
        }
        public UpdateStudentViewModel GetRequestDto(int id)
        {
            var user = this.usersRepository.GetById(id);
            return this.mapper.Map<UpdateStudentViewModel>(user);
        }

        public IEnumerable<UserResponseDto> Get(UserQueryParameters queryParameters)
        {
            var users = this.usersRepository.Get(queryParameters);
            var dtos = new List<UserResponseDto>();
            foreach (var user in users)
            {
                dtos.Add(this.mapper.Map<UserResponseDto>(user));
            }

            return dtos;
        }

        public List<User> GetAllStudents()
        {
            return this.usersRepository.GetAllStudents();
        }

        public UserRequestDto Update(int id, UserRequestDto dto)
        {
            var user = this.usersRepository.Update(id, dto);

            return this.mapper.Map<UserRequestDto>(user);
        }

        public User Delete(int id)
        {
            return this.usersRepository.Delete(id);
        }

        public bool UserExists(string email)
        {
            return this.usersRepository.UserExists(email);
        }

        public bool ValidateUser(UserCred userCred)
        {
            return this.usersRepository.ValidateCredentials(userCred);
        }

        public User GetUserByEmail(string email)
        {
            return this.usersRepository.GetByEmail(email);
        }

        public void Upload(UpdateStudentViewModel viewModel, User user)
        {
            string wwwRootPath = this.hostEnvironment.WebRootPath;
            viewModel.ProfilePhoto.ProfilePhotoId = user.Id;
            viewModel.ProfilePhoto.Name = user.Email.ToString() + ".jpg";
            string path = Path.Combine(wwwRootPath + "/Photos", viewModel.ProfilePhoto.Name);

            if (File.Exists(path)) // Deletes photo from folder
            {
                File.Delete(path);
            }

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                viewModel.ProfilePhoto.PhotoFile.CopyTo(fileStream);
            }
        }
    }
}
