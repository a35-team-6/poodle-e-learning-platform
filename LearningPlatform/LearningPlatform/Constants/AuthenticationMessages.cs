﻿namespace LearningPlatform.Constants
{
    public static class AuthenticationMessages
    {
        public const string MissmatchingPasswords = "The password and the confirmation password do not match";
        public const string AlreadyTakenEmail = "This email address is already taken.";
        public const string SuccessfulRegistration = "Your registration was successful";
        public const string WrongCredentials = "Wrong username or password!";
        public const string LoggedIn = "Welcome, {0}";
        public const string LoggedOut = "You just logged-out";
    }
}
