﻿namespace LearningPlatform.Helpers
{
    public static class ExceptionMessages
    {
        public static string EntityNotFoundExceptionMessage = "Entity with id {0} was not found!";
        public static string EntityNotFoundExceptionMessageEmail = "Entity with email {0} was not found!";
        public static string UserExistExceptionMessage = "User with this email is already registered!";

        public static string NotImplementedExceptionMessage = "This operation is not available at the moment.";
        public static string NullReferenceExceptionMessage = "Value that you reffer to is Null!";
        public static string EntityNotFoundMessage = "The entity that you are looking for can not be found!";
        public static string ArgumentNullExceptionMessage = "Please login or register first!";
        public static string DefaultExceptionMessage = "ERROR! Please contact support for help @ snikolovdev@outlook.com or stanislav_ik@abv.bg";
    }
}
