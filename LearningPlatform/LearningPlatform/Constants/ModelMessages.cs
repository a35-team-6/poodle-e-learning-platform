﻿namespace LearningPlatform.Constants
{
    public static class ModelMessages
    {
        public const string InvalidLenght = "The {0} field must be between {2} and {1} charachters";
        public static string SuccessfulCreate = "The {0} was created successfully";
        public static string SuccessfulUpdate = "The {0} was updated successfully";
        public const string SuccessfulDelete = "The {0} was deleted successfuly";
        public const string SuccessfulyUnenrolled = "Successfuly unenrolled";
    }
}
