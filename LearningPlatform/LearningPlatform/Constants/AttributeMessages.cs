﻿namespace LearningPlatform.Constants
{
    public class AttributeMessages
    {
        public const string RequiredField = "The {0} field is required.";
        public const string MinLength = "The {0} field must be atleast {1} characters.";
        public const string MaxLength = "The {0} field must be less than {1} characters.";
        public const string LengthRange = "The {0} must be between {2} and {1} characters.";
        public const string InvalidEmail = "Invalid Email Address";
    }
}
