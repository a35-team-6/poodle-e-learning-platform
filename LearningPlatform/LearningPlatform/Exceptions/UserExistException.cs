﻿using System;

namespace LearningPlatform.Exceptions
{
    public class UserExistException : ApplicationException
    {
        public UserExistException()
        {

        }

        public UserExistException(string message)
            : base(message)
        {

        }
    }
}
