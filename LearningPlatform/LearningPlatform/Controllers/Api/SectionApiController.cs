﻿using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Controllers.Api;
using LearningPlatform.Exceptions;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers
{
    public class SectionApiController : ApiController
    {
        private readonly ISectionService sectionService;

        public SectionApiController(ISectionService sectionService)
        {
            this.sectionService = sectionService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] SectionRequestDto sectionDto)
        {
            try
            {
                SectionResponseDto returnDto = sectionService.Create(sectionDto);

                return StatusCode(StatusCodes.Status201Created, returnDto);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                SectionResponseDto returnDto = sectionService.GetById(id);

                return StatusCode(StatusCodes.Status200OK, returnDto);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpGet]
        public IActionResult Get([FromBody] SectionQueryParameters filterParameters)
        {
            try
            {
                var sections = sectionService.Get(filterParameters);
                return StatusCode(StatusCodes.Status200OK, sections);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromQuery] SectionRequestDto sectionRequestDto)
        {
            try
            {
                var sectionToUpdate = sectionService.Update(id, sectionRequestDto);

                return StatusCode(StatusCodes.Status200OK, sectionToUpdate);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var section = sectionService.GetSectionById(id);
                this.sectionService.Delete(section);

                return StatusCode(StatusCodes.Status200OK);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
        }
    }
}
