﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Exceptions;
using LearningPlatform.Models;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers.Api
{
    [Display(Name = "controller")]
    public class CourseApiController : ApiController
    {
        private readonly ICourseService courseService;
        private readonly IUserService userService;

        public CourseApiController(ICourseService courseService, IUserService userService)
        {
            this.courseService = courseService;
            this.userService = userService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] CourseRequestDto courseRequestDto)
        {
            try
            {
                CourseResponseDto courseResponseDto = courseService.Create(courseRequestDto);

                return StatusCode(StatusCodes.Status201Created, courseResponseDto);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                CourseResponseDto courseResponseDto = courseService.GetById(id);

                return StatusCode(StatusCodes.Status200OK, courseResponseDto);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpGet]
        public IActionResult Get([FromQuery] CourseQueryParameters filterParameters)
        {
            try
            {
                var courses = courseService.Get(filterParameters);
                return StatusCode(StatusCodes.Status200OK, courses);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] CourseRequestDto courseRequestDto)
        {
            try
            {
                var updatedCourse = courseService.Update(id, courseRequestDto);

                return StatusCode(StatusCodes.Status200OK, updatedCourse);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var courseToDelete = courseService.GetById(id);
                this.courseService.Delete(id);

                return StatusCode(StatusCodes.Status200OK, courseToDelete);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpPut("enroll/{id}")]
        public IActionResult Enroll(int id, [FromBody] List<int> studentsId)
        {
            var studentsToEnroll = new List<User>();

            foreach (var studentId in studentsId)
            {
                var userResponseDto = userService.GetById(studentId);
                var user = userService.GetUserByEmail(userResponseDto.Email);
                user.IsChecked = true;
                studentsToEnroll.Add(user);
            }

            this.courseService.Enroll(id, studentsToEnroll);

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPut("unenroll/{id}")]
        public IActionResult Unenroll(int id, [FromBody] List<int> studentsId)
        {
            var studentsToUnenroll = new List<User>();

            foreach (var studentId in studentsId)
            {
                var userResponseDto = userService.GetById(studentId);
                var user = userService.GetUserByEmail(userResponseDto.Email);
                user.IsChecked = true;
                studentsToUnenroll.Add(user);
            }

            this.courseService.Unenroll(id, studentsToUnenroll);

            return StatusCode(StatusCodes.Status200OK);
        }
    }
}
