﻿using System;
using System.Net;

using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Exceptions;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers.Api
{
    public class ErrorApiController : Controller
    {
        [AllowAnonymous]
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult ErrorPage()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var statusCode = exception.Error.GetType().Name switch
            {
                nameof(NotImplementedException) => HttpStatusCode.NotImplemented,
                nameof(NullReferenceException) => HttpStatusCode.NotFound,
                nameof(EntityNotFoundException) => HttpStatusCode.NotFound,
                nameof(ArgumentNullException) => HttpStatusCode.NetworkAuthenticationRequired,
                _ => HttpStatusCode.ServiceUnavailable,
            };

            var error = new ErrorPageViewModel { StatusCode = (int)statusCode, ErrorMessage = exception.Error.Message };
            return Problem(detail: exception.Error.Message, statusCode: (int)statusCode);
        }
    }
}