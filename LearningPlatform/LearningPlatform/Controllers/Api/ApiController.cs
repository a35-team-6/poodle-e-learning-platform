﻿using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers.Api
{
    [ApiController]
    [Route("api/platform/[controller]")]
    public class ApiController : ControllerBase
    {
    }
}
