﻿using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Exceptions;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers.Api
{
    public class UserApiController : ApiController
    {
        private readonly IUserService usersService;

        public UserApiController(IUserService usersService)
        {
            this.usersService = usersService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserRequestDto userDto)
        {
            try
            {
                UserResponseDto returnDto = usersService.Create(userDto);

                return StatusCode(StatusCodes.Status201Created, returnDto);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                UserResponseDto returnDto = usersService.GetById(id);

                return StatusCode(StatusCodes.Status200OK, returnDto);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpGet]
        public IActionResult Get([FromQuery] UserQueryParameters filterParameters)
        {
            try
            {
                var users = usersService.Get(filterParameters);
                return StatusCode(StatusCodes.Status200OK, users);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UserRequestDto dto)
        {
            try
            {
                var updatedUser = usersService.Update(id, dto);

                return StatusCode(StatusCodes.Status200OK, updatedUser);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                UserResponseDto userToDelete = usersService.GetById(id);
                this.usersService.Delete(id);

                return StatusCode(StatusCodes.Status200OK, userToDelete);
            }
            catch (UnauthorizedOperationException e)// If user isn't owner of the profile
            {
                return StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
    }
}
