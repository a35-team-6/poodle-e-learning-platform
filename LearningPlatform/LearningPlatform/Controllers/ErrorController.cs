﻿using System;
using System.Net;

using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Exceptions;
using LearningPlatform.Helpers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers
{
    public class ErrorController : Controller
    {
        [AllowAnonymous]
        [HttpGet("ErrorPage")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult ErrorPage()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var statusCode = exception.Error.GetType().Name switch
            {
                nameof(NotImplementedException) => HttpStatusCode.NotImplemented,
                nameof(NullReferenceException) => HttpStatusCode.NotFound,
                nameof(EntityNotFoundException) => HttpStatusCode.NotFound,
                nameof(ArgumentNullException) => HttpStatusCode.NetworkAuthenticationRequired,
                _ => HttpStatusCode.ServiceUnavailable,
            };

            string errorMessage = exception.Error.GetType().Name switch
            {
                nameof(NotImplementedException) => ExceptionMessages.NotImplementedExceptionMessage,
                nameof(NullReferenceException) => ExceptionMessages.NullReferenceExceptionMessage,
                nameof(EntityNotFoundException) => ExceptionMessages.EntityNotFoundMessage,
                nameof(ArgumentNullException) => ExceptionMessages.ArgumentNullExceptionMessage,
                _ => ExceptionMessages.DefaultExceptionMessage,
            };

            var error = new ErrorPageViewModel { StatusCode = (int)statusCode, ErrorMessage = errorMessage };
            return View(model: error);
        }
    }
}