﻿using System.Linq;

using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Constants;
using LearningPlatform.Identity;
using LearningPlatform.Models;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IAuthorizationHelper authorizationHelper;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IAuthorizationHelper authorizationHelper, IMapper mapper)
        {
            this.userService = userService;
            this.authorizationHelper = authorizationHelper;
            this.mapper = mapper;
        }

        [NoDirectAccess]
        [OnlyTeachersAccess]
        public IActionResult Students(UserQueryParameters queryParameters)
        {
            var students = userService.Get(queryParameters).ToList();
            return View(model: students);
        }

        public IActionResult Teachers(UserQueryParameters queryParameters)
        {
            var teachers = userService.Get(queryParameters).ToList();
            return View(model: teachers);
        }

        [NoDirectAccess]
        [HttpGet]
        public IActionResult ProfilePage(int id)
        {
            var user = GetUserDetails();
            return View(model: user);
        }

        [NoDirectAccess]
        public IActionResult Update()
        {
            var user = GetUserDetails();
            var viewModel = userService.GetRequestDto(user.Id);
            return View(model: viewModel);
        }

        [NoDirectAccess]
        [HttpPost]
        public IActionResult Update([FromForm] UpdateStudentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(model: viewModel);
            }

            if (viewModel.Password != viewModel.ConfirmPassword)
            {
                this.ModelState.AddModelError("ConfirmPassword", AuthenticationMessages.MissmatchingPasswords);

                return View(model: viewModel);
            }

            var userToUpdate = GetUserDetails();
            viewModel.Email = userToUpdate.Email;

            var mapped = mapper.Map<UserRequestDto>(viewModel);
            this.userService.Update(userToUpdate.Id, mapped);

            string message = string.Format(ModelMessages.SuccessfulUpdate, nameof(User));
            this.TempData["Message"] = message;
            return RedirectToAction(actionName: "ProfilePage", controllerName: "User", new { id = userToUpdate.Id });
        }

        [NoDirectAccess]
        [HttpPost]
        public IActionResult Delete()
        {
            var userToDelete = authorizationHelper.GetUserFromToken(HttpContext);
            this.userService.Delete(userToDelete.Id);

            string message = string.Format(ModelMessages.SuccessfulDelete, nameof(User));
            this.TempData["Message"] = message;

            return RedirectToAction(controllerName: "Authentication", actionName: "Logout");
        }

        [NoDirectAccess]
        [HttpPost("upload")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult Upload([FromForm] UpdateStudentViewModel viewModel)
        {
            var user = authorizationHelper.GetUserFromToken(HttpContext);
            if (viewModel.ProfilePhoto != null && viewModel.ProfilePhoto.PhotoFile.Length > 0)
            {
                this.userService.Upload(viewModel, user);

                return RedirectToAction(actionName: "ProfilePage", controllerName: "User", new { id = user.Id });
            }

            return View(viewModel);
        }

        private User GetUserDetails()
        {
            return authorizationHelper.GetUserFromToken(HttpContext);
        }
    }
}
