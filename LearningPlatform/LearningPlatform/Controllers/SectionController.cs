﻿using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Constants;
using LearningPlatform.Identity;
using LearningPlatform.Models;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers
{
    [NoDirectAccess]
    public class SectionController : Controller
    {
        private readonly ISectionService sectionService;
        private readonly IAuthorizationHelper authHelper;

        public SectionController(ISectionService sectionService, IAuthorizationHelper authHelper)
        {
            this.sectionService = sectionService;
            this.authHelper = authHelper;
        }

        public IActionResult Index([FromRoute] int id)
        {
            var section = sectionService.GetSectionById(id);

            var user = authHelper.GetUserFromToken(HttpContext);
            HttpContext.Session.SetString("CurrentUserRole", user.Role.ToString());

            return View(model: section);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(int id, SectionRequestDto requestDto)
        {
            requestDto.CourseId = id;

            if (!ModelState.IsValid)
            {
                return View(model: requestDto);
            }

            this.sectionService.Create(requestDto);

            string message = string.Format(ModelMessages.SuccessfulCreate, nameof(Section));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "ReadMore", controllerName: "Course", new { id = requestDto.CourseId });
        }

        public IActionResult Edit(int id)
        {
            var requestDto = sectionService.GetRequestDto(id);

            return View(model: requestDto);
        }

        [HttpPost]
        public IActionResult Edit(int id, SectionRequestDto requestDto)
        {
            var section = sectionService.GetById(id);

            requestDto.CourseId = section.CourseId;

            if (!ModelState.IsValid)
            {
                return View(requestDto);
            }

            this.sectionService.Update(id, requestDto);

            string message = string.Format(ModelMessages.SuccessfulUpdate, nameof(Section));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "Index", controllerName: "Section", new { id = id });
        }

        public IActionResult Delete([FromRoute] int id)
        {
            var section = sectionService.GetSectionById(id);
            var courseId = section.CourseId;
            this.sectionService.Delete(section);

            string message = string.Format(ModelMessages.SuccessfulDelete, nameof(Section));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "ReadMore", controllerName: "Course", new { id = courseId });
        }
    }
}
