﻿using System.Linq;

using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Constants;
using LearningPlatform.Identity;
using LearningPlatform.Identity.Authentication;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LearningPlatform.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IMapper mapper;
        private readonly IUserService userService;

        private readonly IJwtAuthenticationManager authManager;
        private readonly IAuthorizationHelper authorizationHelper;

        public AuthenticationController(IMapper mapper, IUserService userService, IJwtAuthenticationManager authManager, IAuthorizationHelper authorizationHelper)
        {
            this.mapper = mapper;
            this.userService = userService;

            this.authManager = authManager;
            this.authorizationHelper = authorizationHelper;
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(UserCred userCred)
        {
            var token = authManager.Authenticate(userCred, userService);

            if (token == null)
            {
                return Unauthorized();
            }

            this.HttpContext.Session.SetString("JWToken", token);

            return Ok(token);
        }

        public IActionResult Register()
        {
            var viewModel = new RegisterViewModel();

            return View(model: viewModel);
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(model: viewModel);
            }

            if (viewModel.Password != viewModel.ConfirmPassword)
            {
                ModelState.AddModelError("ConfirmPassword", AuthenticationMessages.MissmatchingPasswords);

                return View(model: viewModel);
            }

            var userExist = userService.UserExists(viewModel.Email);

            if (userExist)
            {
                ModelState.AddModelError("Email", AuthenticationMessages.AlreadyTakenEmail);

                return View(model: viewModel);
            }

            var user = mapper.Map<UserRequestDto>(viewModel);
            this.userService.Create(user);

            this.TempData["Message"] = AuthenticationMessages.SuccessfulRegistration;
            return RedirectToAction(controllerName: "home", actionName: "index");
        }

        [HttpGet]
        public IActionResult Login()
        {
            var viewModel = new LoginViewModel();

            return View(model: viewModel);
        }

        [HttpPost]
        public IActionResult Login([FromForm] LoginViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(model: viewModel);
            }

            var userCred = mapper.Map<UserCred>(viewModel);

            Authenticate(userCred);

            var token = HttpContext.Session.GetString("JWToken");

            if (string.IsNullOrEmpty(token))
            {
                TempData["Message"] = AuthenticationMessages.WrongCredentials;
                return View();
            }

            var user = authorizationHelper.GetUserFromToken(HttpContext);

            HttpContext.Session.SetString("CurrentUser", user.FirstName);
            TempData["Message"] = string.Format(AuthenticationMessages.LoggedIn, user.FirstName);

            if (user.Role == Models.Enums.Role.Teacher)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Course");
            }

            return RedirectToAction(actionName: "ProfilePage", controllerName: "User", new { id = user.Id });
        }

        [HttpGet("logout")]
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            this.HttpContext.Session.Remove("JWToken");
            if (!TempData.Any())
            {
                TempData["Message"] = AuthenticationMessages.LoggedOut;
            }

            return RedirectToAction(controllerName: "home", actionName: "index");
        }
    }
}
