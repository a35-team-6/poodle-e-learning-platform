﻿using System.Collections.Generic;

using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Constants;
using LearningPlatform.Identity;
using LearningPlatform.Models;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LearningPlatform.Controllers
{
    public class CourseController : Controller
    {
        private readonly IUserService userService;
        private readonly ICourseService courseService;
        private readonly IAuthorizationHelper authorizationHelper;

        public CourseController(ICourseService courseService, IUserService userService, IAuthorizationHelper authorizationHelper)
        {
            this.userService = userService;
            this.authorizationHelper = authorizationHelper;
            this.courseService = courseService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var courses = courseService.GetAllCourses();
            var user = authorizationHelper.GetUserFromToken(HttpContext);
            var viewModel = new CourseStudentViewModel() { Courses = courses, User = user };

            return View(model: viewModel);
        }

        [NoDirectAccess]
        [HttpGet]
        public IActionResult Create()
        {
            var viewModel = new CouresViewModel();
            this.GetAllCourseTypes(viewModel);

            return View(viewModel);
        }

        [NoDirectAccess]
        [HttpPost]
        public IActionResult Create(CouresViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                GetAllCourseTypes(viewModel);

                return View(viewModel);
            }

            courseService.Create(viewModel);

            string message = string.Format(ModelMessages.SuccessfulCreate, nameof(Course));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "Index", controllerName: "Course");
        }

        [NoDirectAccess]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var viewModel = courseService.GetViewModelById(id);
            this.GetAllCourseTypes(viewModel);

            return View(model: viewModel);
        }

        [NoDirectAccess]
        [HttpPost]
        public IActionResult Edit(int id, CouresViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                GetAllCourseTypes(viewModel);
                return View(viewModel);
            }

            this.courseService.Update(id, viewModel);

            string message = string.Format(ModelMessages.SuccessfulUpdate, nameof(Course));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "ReadMore", controllerName: "Course", new { id = id });
        }

        [NoDirectAccess]
        [HttpGet]
        public IActionResult ReadMore(int id)
        {
            var course = courseService.GetCourse(id);
            var user = authorizationHelper.GetUserFromToken(HttpContext);
            var viewModel = new ReadMoreViewModel() { Course = course, User = user };

            if (user.Role.ToString() == "Student" && !course.Students.Contains(user))
            {
                user.IsChecked = true;

                var userList = new List<User> { user };
                this.courseService.Enroll(id, userList);
            }

            return View(model: viewModel);
        }

        [NoDirectAccess]
        public IActionResult Delete(int id)
        {
            this.courseService.Delete(id);
            string message = string.Format(ModelMessages.SuccessfulDelete, nameof(Course));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "Index", controllerName: "Course");
        }

        [NoDirectAccess]
        public IActionResult UnenrollStudent(int id)
        {
            var user = authorizationHelper.GetUserFromToken(HttpContext);
            user.IsChecked = true;
            var userList = new List<User> { user };
            this.courseService.Unenroll(id, userList);

            string message = string.Format(ModelMessages.SuccessfulyUnenrolled, nameof(Course));
            this.TempData["Message"] = message;

            return RedirectToAction(actionName: "Index", controllerName: "Course");
        }

        [NoDirectAccess]
        public IActionResult EnrollStudents(int id)
        {
            var viewModel = new EnrollViewModel();
            viewModel.CourseId = id;
            viewModel.RegisteredStudents.AddRange(courseService.GetRegisteredStudents(id));
            viewModel.UnregisteredStudents.AddRange(courseService.GetUnregisteredStudents(id));

            return View(viewModel);
        }

        [NoDirectAccess]
        [HttpPost]
        public IActionResult EnrollStudents(EnrollViewModel viewModel)
        {
            this.courseService.Enroll(viewModel.CourseId, viewModel.UnregisteredStudents);
            this.courseService.Unenroll(viewModel.CourseId, viewModel.RegisteredStudents);

            return RedirectToAction(actionName: "EnrollStudents", controllerName: "Course", new { id = viewModel.CourseId });
        }

        private void GetAllCourseTypes(CouresViewModel viewModel)
        {
            var courseTypes = courseService.Get();

            viewModel.CourseTypes = new SelectList(courseTypes);
        }
    }
}
