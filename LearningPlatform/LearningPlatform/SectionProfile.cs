﻿using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Models;

namespace LearningPlatform
{
    public class SectionProfile : Profile
    {
        public SectionProfile()
        {
            CreateMap<Section, SectionRequestDto>()
                .ReverseMap();

            CreateMap<Section, SectionResponseDto>()
                .ReverseMap();
        }
    }
}
