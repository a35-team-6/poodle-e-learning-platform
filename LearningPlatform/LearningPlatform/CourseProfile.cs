﻿using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Models;

namespace LearningPlatform
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            CreateMap<Course, CourseRequestDto>()
                .ReverseMap();

            CreateMap<Course, CourseResponseDto>()
                .ReverseMap();

            CreateMap<CourseResponseDto, CouresViewModel>()
                .ReverseMap();
        }
    }
}
