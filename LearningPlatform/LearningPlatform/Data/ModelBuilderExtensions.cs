﻿using System;
using System.Collections.Generic;

using LearningPlatform.Models;
using LearningPlatform.Models.Enums;

using Microsoft.EntityFrameworkCore;

namespace LearningPlatform.Data
{
    // TODO: Copy methods in test project.
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Seeds the database with teachers
            List<User> users = GetUsers();

            modelBuilder.Entity<User>()
                        .HasData(users);

            // Seeds the database with courses
            List<Course> courses = GetCourses();

            modelBuilder.Entity<Course>()
                        .HasData(courses);

            // Seeds the database with sections
            List<Section> sections = GetSections();

            modelBuilder.Entity<Section>()
                        .HasData(sections);
        }

        public static List<Section> GetSections()
        {
            var sections = new List<Section>();
            sections.Add(new Section() { Id = 1, Title = "Data types", Content = "Basic data types.", Order = 1, CreationTime = System.DateTime.Now, CourseId = 1 });
            sections.Add(new Section() { Id = 2, Title = "While-cycle", Content = "Learn how to use whyle-cycles in C#", Order = 2, CreationTime = DateTime.Now, CourseId = 2 });
            sections.Add(new Section() { Id = 3, Title = "For-cycle", Content = "Learn how to use for-cycles in C#", Order = 3, CreationTime = DateTime.Now, CourseId = 3 });
            sections.Add(new Section() { Id = 4, Title = "Data types", Content = "Basic data types.", Order = 1, CreationTime = System.DateTime.Now, CourseId = 4 });
            sections.Add(new Section() { Id = 5, Title = "While-cycle", Content = "Learn how to use whyle-cycles in C#", Order = 2, CreationTime = DateTime.Now, CourseId = 5 });
            sections.Add(new Section() { Id = 6, Title = "For-cycle", Content = "Learn how to use for-cycles in C#", Order = 3, CreationTime = DateTime.Now, CourseId = 6 });
            sections.Add(new Section() { Id = 7, Title = "Data types", Content = "Basic data types.", Order = 1, CreationTime = System.DateTime.Now, CourseId = 7 });
            sections.Add(new Section() { Id = 8, Title = "While-cycle", Content = "Learn how to use whyle-cycles in C#", Order = 2, CreationTime = DateTime.Now, CourseId = 1 });
            sections.Add(new Section() { Id = 9, Title = "For-cycle", Content = "Learn how to use for-cycles in C#", Order = 2, CreationTime = DateTime.Now, CourseId = 2 });
            sections.Add(new Section() { Id = 10, Title = "Data types", Content = "Basic data types.", Order = 1, CreationTime = System.DateTime.Now, CourseId = 3 });
            sections.Add(new Section() { Id = 11, Title = "While-cycle", Content = "Learn how to use whyle-cycles in C#", Order = 2, CreationTime = DateTime.Now, CourseId = 4 });
            sections.Add(new Section() { Id = 12, Title = "SOLID", Content = "Learn how to use for-cycles in C#", Order = 3, CreationTime = DateTime.Now, CourseId = 5 });
            sections.Add(new Section() { Id = 13, Title = "OOP", Content = "Learn how to use for-cycles in C#", Order = 3, CreationTime = DateTime.Now, CourseId = 6 });
            sections.Add(new Section() { Id = 14, Title = "DSA", Content = "Learn how to use for-cycles in C#", Order = 3, CreationTime = DateTime.Now, CourseId = 7 });
            return sections;
        }

        public static List<Course> GetCourses()
        {
            var courses = new List<Course>();

            //Public courses:
            courses.Add(new Course()
            {
                Id = 1,
                Title = "C# Basics",
                Description = "First steps in coding!",
                CourseType = CourseType.Public,
            });
            courses.Add(new Course()
            {
                Id = 2,
                Title = "Java Basics",
                Description = "It's time to learn more!",
                CourseType = CourseType.Public
            });
            courses.Add(new Course()
            {
                Id = 3,
                Title = "Java Script Basics",
                Description = "Throw yourself into knowledge!",
                CourseType = CourseType.Public
            });
            courses.Add(new Course()
            {
                Id = 4,
                Title = "Python Basics",
                Description = "First steps in coding!",
                CourseType = CourseType.Public,
            });

            //Private courses:
            courses.Add(new Course()
            {
                Id = 5,
                Title = "C# Fundamentals",
                Description = "It's time to learn more!",
                CourseType = CourseType.Private
            });
            courses.Add(new Course()
            {
                Id = 6,
                Title = "Java Fundamentals",
                Description = "Throw yourself into knowledge!",
                CourseType = CourseType.Private
            });
            courses.Add(new Course()
            {
                Id = 7,
                Title = "Java Script Fundamentals",
                Description = "Throw yourself into knowledge!",
                CourseType = CourseType.Private
            });
            courses.Add(new Course()
            {
                Id = 8,
                Title = "Python Fundamentals",
                Description = "Throw yourself into knowledge!",
                CourseType = CourseType.Private
            });
            return courses;
        }

        public static List<User> GetUsers()
        {
            var users = new List<User>();
            users.Add(new User() { Id = 1, Email = "snikolov@telerik.com", FirstName = "Stefan", LastName = "Nikolov", Password = "123123", Role = Role.Teacher, Info = "Experienced and with deep knowlage of C#, SQL, ASP.NET. Eager to learn and to teach." });
            users.Add(new User() { Id = 2, Email = "skonstantinov@telerik.com", FirstName = "Stanislav", LastName = "Konstantinov", Password = "123123", Role = Role.Teacher, Info = "You can learn a lot from our top teacher and CTO." });
            users.Add(new User() { Id = 3, Email = "pminkov@telerik.com", FirstName = "Petar", LastName = "Minkov", Password = "123123", Role = Role.Student, Info = "Deep knowlage of testing and Java Script." });
            users.Add(new User() { Id = 4, Email = "georgiev@telerik.com", FirstName = "Georgi", LastName = "Georgiev", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 5, Email = "ivanov@telerik.com", FirstName = "Ivan", LastName = "Ivanov", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 6, Email = "petrov@telerik.com", FirstName = "Georgi", LastName = "Petrov", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 7, Email = "atanasova@telerik.com", FirstName = "Antoniya", LastName = "Atanasova", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 8, Email = "stamenova@telerik.com", FirstName = "Petiya", LastName = "Stamenova", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 9, Email = "vakov@telerik.com", FirstName = "Mihail", LastName = "Vakov", Password = "123123", Role = Role.Student });
            users.Add(new User() { Id = 10, Email = "toskov@telerik.com", FirstName = "Slavi", LastName = "Toskov", Password = "123123", Role = Role.Student });
            return users;
        }
    }
}
