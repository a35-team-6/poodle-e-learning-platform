﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace LearningPlatform.Identity
{
    public class OnlyTeachersAccess : System.Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context) { }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var authorizationHelper = context.HttpContext.RequestServices.GetService<IAuthorizationHelper>();
            var user = authorizationHelper.GetUserFromToken(context.HttpContext);

            if (user.Role != Models.Enums.Role.Teacher)
            {
                context.Result = new RedirectResult("http://localhost:5000/");
            }
        }
    }
}
