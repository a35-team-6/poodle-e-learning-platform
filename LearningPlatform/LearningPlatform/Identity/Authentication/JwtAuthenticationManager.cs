﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using LearningPlatform.Models.Enums;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace LearningPlatform.Identity.Authentication
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly string key;

        public JwtAuthenticationManager(string key)
        {
            this.key = key;
        }

        public string Authenticate(UserCred userCred, [FromServices] IUserService userService)
        {
            var result = userService.ValidateUser(userCred);
            if (!result)
            {
                return null;
            }

            Claim secondClaim = new Claim(ClaimTypes.Role, Role.Teacher.ToString());

            var currentUser = userService.GetUserByEmail(userCred.Email);
            if (currentUser.Role == Role.Student)
            {
                secondClaim = new Claim(ClaimTypes.Role, Role.Student.ToString());
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.ASCII.GetBytes(key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, userCred.Email),
                    secondClaim
                }),
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials =
                    new SigningCredentials(
                    new SymmetricSecurityKey(tokenKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
