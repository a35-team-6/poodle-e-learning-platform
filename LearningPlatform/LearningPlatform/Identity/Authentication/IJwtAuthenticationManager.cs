﻿using LearningPlatform.Services.Interfaces;

namespace LearningPlatform.Identity.Authentication
{
    public interface IJwtAuthenticationManager
    {
        string Authenticate(UserCred userCred, IUserService userService);
    }
}
