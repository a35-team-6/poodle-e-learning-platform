﻿using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

public class NoDirectAccessAttribute : System.Attribute, IActionFilter
{
    public void OnActionExecuted(ActionExecutedContext context) { }

    public void OnActionExecuting(ActionExecutingContext context)
    {
        var isAuthenticated = context.HttpContext.Session.Keys.Contains("JWToken");

        if (!isAuthenticated)
        {
            context.Result = new RedirectResult("http://localhost:5000/Authentication/Login");
        }
    }
}
