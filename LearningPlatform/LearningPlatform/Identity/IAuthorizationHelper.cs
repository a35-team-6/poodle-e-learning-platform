﻿using LearningPlatform.Models;

using Microsoft.AspNetCore.Http;

namespace LearningPlatform.Identity
{
    public interface IAuthorizationHelper
    {
        User GetUserFromToken(HttpContext httpContext);
    }
}