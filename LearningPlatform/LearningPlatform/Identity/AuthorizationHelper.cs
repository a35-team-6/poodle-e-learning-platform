﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;

using LearningPlatform.Models;
using LearningPlatform.Services.Interfaces;

using Microsoft.AspNetCore.Http;

namespace LearningPlatform.Identity
{
    public class AuthorizationHelper : IAuthorizationHelper
    {
        private readonly IUserService userService;

        public AuthorizationHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public User GetUserFromToken(HttpContext httpContext)
        {
            var token = httpContext.Session.GetString("JWToken");
            var jwtHandler = new JwtSecurityTokenHandler();
            var tokenContent = jwtHandler.ReadToken(token) as JwtSecurityToken;
            var email = tokenContent.Claims.First(claim => claim.Type == "email").Value;
            var user = userService.GetUserByEmail(email);

            return user;
        }
    }
}
