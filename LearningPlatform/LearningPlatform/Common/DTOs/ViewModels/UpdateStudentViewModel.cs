﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;
using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class UpdateStudentViewModel
    {
        [Display(Name = "Email address")]
        [EmailAddress(ErrorMessage = AttributeMessages.InvalidEmail)]
        public string Email { get; set; }

        [Display(Name = "First name")]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string LastName { get; set; }

        [StringLength(16, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public ProfilePhoto ProfilePhoto { get; set; }
    }
}
