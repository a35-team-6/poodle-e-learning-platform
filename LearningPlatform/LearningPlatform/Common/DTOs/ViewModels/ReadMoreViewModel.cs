﻿
using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class ReadMoreViewModel
    {
        public User User { get; set; }

        public Course Course { get; set; }
    }
}
