﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Common.DTOs.Requests;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class RegisterViewModel : UserRequestDto
    {
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
