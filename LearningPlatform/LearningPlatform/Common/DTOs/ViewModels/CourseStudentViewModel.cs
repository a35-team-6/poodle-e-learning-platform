﻿using System.Collections.Generic;

using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class CourseStudentViewModel
    {
        public List<Course> Courses { get; set; } = new List<Course>();

        public User User { get; set; }
    }
}
