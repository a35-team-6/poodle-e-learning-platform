﻿using LearningPlatform.Common.DTOs.Requests;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class CouresViewModel : CourseRequestDto
    {
        public SelectList CourseTypes { get; set; }
    }
}
