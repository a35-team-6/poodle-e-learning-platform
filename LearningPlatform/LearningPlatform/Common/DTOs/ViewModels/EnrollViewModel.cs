﻿using System.Collections.Generic;

using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class EnrollViewModel
    {
        public int CourseId { get; set; }

        public List<User> RegisteredStudents { get; set; } = new List<User>();

        public List<User> UnregisteredStudents { get; set; } = new List<User>();
    }
}
