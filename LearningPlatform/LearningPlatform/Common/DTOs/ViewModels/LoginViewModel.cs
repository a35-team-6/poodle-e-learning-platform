﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class LoginViewModel
    {
        [Display(Name = "Email address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [EmailAddress(ErrorMessage = AttributeMessages.InvalidEmail)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(16, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string Password { get; set; }
    }
}
