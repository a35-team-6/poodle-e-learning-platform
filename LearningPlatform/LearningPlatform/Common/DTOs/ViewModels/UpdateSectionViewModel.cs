﻿using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class UpdateSectionViewModel
    {
        public Section Section { get; set; }

        public User User { get; set; }
    }
}
