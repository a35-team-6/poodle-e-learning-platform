﻿namespace LearningPlatform.Common.DTOs.ViewModels
{
    public class ErrorPageViewModel
    {
        public int StatusCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
