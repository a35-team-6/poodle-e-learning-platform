﻿using System.Collections.Generic;
using System.Linq;

using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Models;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Common.DTOs.Requests
{
    public class CourseResponseDto
    {
        public CourseResponseDto() { }

        public CourseResponseDto(Course model)
        {
            Title = model.Title;
            Description = model.Description;
            CourseType = model.CourseType;
            Students = model.Students
                .Select(s => new UserResponseDto())
                .ToList();
            Sections = model.Sections
                .Select(section => new SectionResponseDto())
                .ToList();
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public CourseType CourseType { get; set; }

        public IList<UserResponseDto> Students { get; set; }

        public IList<SectionResponseDto> Sections { get; set; }
    }
}
