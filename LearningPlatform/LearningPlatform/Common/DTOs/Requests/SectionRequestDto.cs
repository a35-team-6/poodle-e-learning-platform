﻿using System;
using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;

namespace LearningPlatform.Common.DTOs.Requests
{
    public class SectionRequestDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(64, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [MinLength(32, ErrorMessage = AttributeMessages.MinLength)]
        public string Content { get; set; }

        [Required]
        [Range(1, 50, ErrorMessage = AttributeMessages.LengthRange)]
        public int Order { get; set; }

        public DateTime CreationTime { get; set; } = DateTime.Now;

        public int CourseId { get; set; }
    }
}
