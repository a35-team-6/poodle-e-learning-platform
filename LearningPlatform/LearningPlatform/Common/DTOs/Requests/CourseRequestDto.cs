﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Common.DTOs.Requests
{
    public class CourseRequestDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [MinLength(4, ErrorMessage = AttributeMessages.MinLength)]
        [MaxLength(50, ErrorMessage = AttributeMessages.MaxLength)]
        [DataType("NVARCHAR")]
        public string Title { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [MinLength(6, ErrorMessage = AttributeMessages.MinLength)]
        [MaxLength(500, ErrorMessage = AttributeMessages.MaxLength)]
        [DataType("NVARCHAR(MAX)")]
        public string Description { get; set; }

        public CourseType CourseType { get; set; }
    }
}
