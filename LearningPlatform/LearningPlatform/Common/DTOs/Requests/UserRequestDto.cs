﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;
using LearningPlatform.Models;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Common.DTOs.Requests
{
    public class UserRequestDto
    {
        [Display(Name = "Email address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [EmailAddress(ErrorMessage = AttributeMessages.InvalidEmail)]
        public string Email { get; set; }

        [Display(Name = "First name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string LastName { get; set; }

        [StringLength(16, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string Password { get; set; }

        public Role Role { get; set; } = Role.Student;

        public ProfilePhoto Photo { get; set; }
    }
}
