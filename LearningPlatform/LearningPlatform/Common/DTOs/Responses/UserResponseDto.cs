﻿using System.Collections.Generic;

using LearningPlatform.Models;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Common.DTOs.Responses
{
    public class UserResponseDto
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Info { get; set; }

        public Role Role { get; set; } = Role.Student;

        public ProfilePhoto Photo { get; set; }

        public IList<Course> Courses { get; set; }

        public bool IsChecked { get; set; }
    }
}
