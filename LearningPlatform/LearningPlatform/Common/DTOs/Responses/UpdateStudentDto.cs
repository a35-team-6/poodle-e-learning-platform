﻿using LearningPlatform.Models;

namespace LearningPlatform.Common.DTOs.Responses
{
    public class UpdateStudentDto
    {
        public string Password { get; set; }

        public ProfilePhoto Photo { get; set; }
    }
}
