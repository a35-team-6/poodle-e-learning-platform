﻿using System;

namespace LearningPlatform.Common.DTOs.Responses
{
    public class SectionResponseDto
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public int Order { get; set; }

        public DateTime CreationTime { get; set; }

        public int CourseId { get; set; }
    }
}
