﻿using System.ComponentModel.DataAnnotations;

namespace LearningPlatform.Common.QuerryParameters
{
    public class SectionQueryParameters
    {
        [StringLength(64, MinimumLength = 4)]
        public string Title { get; set; }

        public int? OrderNumber { get; set; }

        public string SortBy { get; set; }

        public string OrderBy { get; set; }
    }
}
