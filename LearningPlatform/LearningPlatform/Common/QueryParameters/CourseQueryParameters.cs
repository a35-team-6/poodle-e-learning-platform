﻿using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;

namespace LearningPlatform.Common.QuerryParameters
{
    public class CourseQueryParameters
    {
        [MinLength(4, ErrorMessage = AttributeMessages.MinLength)]
        [MaxLength(18, ErrorMessage = AttributeMessages.MaxLength)]
        [DataType("NVARCHAR")]
        public string Title { get; set; }

        public string SortBy { get; set; }

        public string SortOrder { get; set; }
    }
}
