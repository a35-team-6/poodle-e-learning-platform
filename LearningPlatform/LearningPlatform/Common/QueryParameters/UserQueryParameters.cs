﻿
using System.ComponentModel.DataAnnotations;

using LearningPlatform.Constants;

namespace LearningPlatform.Common.QuerryParameters
{
    public class UserQueryParameters
    {
        [Display(Name = "Email address")]
        [EmailAddress(ErrorMessage = AttributeMessages.InvalidEmail)]
        public string Email { get; set; }

        [Display(Name = "First name")]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string LastName { get; set; }

        public string SortBy { get; set; }

        public string OrderBy { get; set; }
    }
}
