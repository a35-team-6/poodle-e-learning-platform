﻿using AutoMapper;

using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Identity.Authentication;

namespace LearningPlatform
{
    public class AuthenticationProfile : Profile
    {
        public AuthenticationProfile()
        {
            CreateMap<LoginViewModel, UserCred>()
               .ReverseMap();
        }
    }
}
