﻿using AutoMapper;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Common.DTOs.ViewModels;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;

namespace LearningPlatform
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserRequestDto>()
                .ReverseMap();

            CreateMap<User, UserResponseDto>()
               .ReverseMap();

            CreateMap<UserQueryParameters, RegisterViewModel>()
                .ReverseMap();

            CreateMap<User, UpdateStudentViewModel>()
                .ReverseMap();

            CreateMap<UpdateStudentViewModel, UserRequestDto>()
                .ReverseMap();
        }
    }
}
