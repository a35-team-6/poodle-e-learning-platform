﻿using System.Collections.Generic;
using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;

namespace LearningPlatform.Repository.Interfaces
{
    public interface ICourseRepository
    {
        Course Create(Course course);

        Course GetById(int id);

        IQueryable<Course> Get(CourseQueryParameters filterParameters);

        Course Update(int id, CourseRequestDto updateParameters);

        Course Delete(int id);

        List<Course> GetAllCourses();

        void Enroll(int courseId, IList<User> students);

        void Unenroll(int courseId, IList<User> students);
    }
}
