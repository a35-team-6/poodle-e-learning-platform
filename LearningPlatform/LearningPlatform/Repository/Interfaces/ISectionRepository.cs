﻿using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Models;

namespace LearningPlatform.Repository.Interfaces
{
    public interface ISectionRepository
    {
        Section Create(Section section);

        Section GetById(int id);

        IQueryable<Section> Get(SectionQueryParameters filerParameters);

        Section Update(int id, SectionRequestDto sectionRequestDto);

        Section Delete(int id);
    }
}
