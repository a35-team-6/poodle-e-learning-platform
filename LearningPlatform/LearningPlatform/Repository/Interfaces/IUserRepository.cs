﻿using System.Collections.Generic;
using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Identity.Authentication;
using LearningPlatform.Models;

namespace LearningPlatform.Repository.Interfaces
{
    public interface IUserRepository
    {
        User Create(User user);

        User GetById(int id);

        bool UserExists(string email);

        bool ValidateCredentials(UserCred userCred);

        User GetByEmail(string email);

        List<User> GetAllStudents();

        IQueryable<User> Get(UserQueryParameters filterParameters);

        User Update(int id, UserRequestDto dto);

        User Delete(int id);
    }
}