﻿using System;
using System.Collections.Generic;
using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Exceptions;
using LearningPlatform.Helpers;
using LearningPlatform.Models;
using LearningPlatform.Repository.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace LearningPlatform.Repository
{
    public class CourseRepository : ICourseRepository
    {
        private readonly ApplicationContext context;

        public CourseRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public Course Create(Course course)
        {
            this.context.Courses.Add(course);
            this.context.SaveChanges();

            return course;
        }

        public Course GetById(int id)
        {
            var course = GetCourses().FirstOrDefault(course => course.Id == id);

            var exceptionMessage = string.Format(ExceptionMessages.EntityNotFoundExceptionMessage, id);
            return course ?? throw new EntityNotFoundException(exceptionMessage);
        }

        public List<Course> GetAllCourses()
        {
            return GetCourses().ToList();
        }

        public IQueryable<Course> Get(CourseQueryParameters queryParameters)
        {
            string title = !string.IsNullOrEmpty(queryParameters.Title) ? queryParameters.Title.ToLowerInvariant() : string.Empty;

            string sortOrder = !string.IsNullOrEmpty(queryParameters.SortOrder) ? queryParameters.SortOrder.ToLowerInvariant() : string.Empty;

            IQueryable<Course> courses = GetCourses();

            courses = FilterByTitle(courses, title);
            courses = SortBy(courses);
            courses = Order(courses, sortOrder);

            return courses;
        }

        public Course Update(int id, CourseRequestDto requestDto)
        {
            var courseToUpdate = GetById(id);

            courseToUpdate.Title = requestDto.Title;
            courseToUpdate.Description = requestDto.Description;
            courseToUpdate.CourseType = requestDto.CourseType;

            this.context.SaveChanges();

            return courseToUpdate;
        }

        public Course Delete(int id)
        {
            var courseToDelete = GetById(id);
            var deletedCourse = context.Courses.Remove(courseToDelete).Entity;
            this.context.SaveChanges();

            return deletedCourse;
        }

        public void Enroll(int courseId, IList<User> students)
        {
            var course = GetById(courseId);

            foreach (var student in students)
            {
                if (student.IsChecked == true)
                {
                    course.Students.Add(student);
                }
            }

            this.context.SaveChanges();
        }

        public void Unenroll(int courseId, IList<User> students)
        {
            var course = GetById(courseId);

            foreach (var student in students)
            {
                if (student.IsChecked == true)
                {
                    var studentToRemove = course.Students.FirstOrDefault(x => x.Email == student.Email);
                    var result = course.Students.Remove(studentToRemove);
                }
            }

            this.context.SaveChanges();
        }

        private IQueryable<Course> GetCourses()
        {
            return context.Courses
                            .Include(course => course.Students)
                            .Include(course => course.Sections);
        }

        private static IQueryable<Course> FilterByTitle(IQueryable<Course> courses, string title)
        {
            return courses.Where(courses => courses.Title.Contains(title));
        }

        private static IOrderedQueryable<Course> SortBy(IQueryable<Course> courses)
        {
            return courses.OrderBy(courses => courses.Title);
        }

        private static IQueryable<Course> Order(IQueryable<Course> courses, string sortOrder)
        {
            return (sortOrder == "desc") ? courses.Reverse() : courses;
        }
    }
}

