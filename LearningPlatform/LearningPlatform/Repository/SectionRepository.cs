﻿using System;
using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Exceptions;
using LearningPlatform.Helpers;
using LearningPlatform.Models;
using LearningPlatform.Repository.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace LearningPlatform.Repository
{
    public class SectionRepository : ISectionRepository
    {
        private readonly ApplicationContext context;

        public SectionRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public Section Create(Section section)
        {
            var createSection = context.Sections.Add(section);
            this.context.SaveChanges();

            return createSection.Entity;
        }

        public Section GetById(int id)
        {
            var result = GetSections().FirstOrDefault(section => section.Id == id);
            var exceptionMessage = string.Format(ExceptionMessages.EntityNotFoundExceptionMessage, id);

            return result ?? throw new EntityNotFoundException(exceptionMessage);
        }

        public IQueryable<Section> Get(SectionQueryParameters queryParameters)
        {
            string title = !string.IsNullOrEmpty(queryParameters.Title) ? queryParameters.Title.ToLowerInvariant() : string.Empty;
            string orderNumber = !string.IsNullOrEmpty(queryParameters.OrderNumber.ToString()) ? queryParameters.OrderNumber.ToString().ToLowerInvariant() : string.Empty;
            string sortCriteria = !string.IsNullOrEmpty(queryParameters.SortBy) ? queryParameters.SortBy.ToLowerInvariant() : string.Empty;
            string orderBy = !string.IsNullOrEmpty(queryParameters.OrderBy) ? queryParameters.OrderBy.ToLowerInvariant() : string.Empty;

            IQueryable<Section> sections = GetSections();

            sections = FilterByTitle(sections, title);
            sections = FilterByOrderNumber(sections, orderNumber);
            sections = SortBy(sections, sortCriteria);
            sections = OrderBy(sections, orderBy);

            return sections;
        }

        public Section Update(int id, SectionRequestDto sectionRequestDto)
        {
            var sectionToUpdate = GetById(id);

            sectionToUpdate.Title = sectionRequestDto.Title;
            sectionToUpdate.Content = sectionRequestDto.Content;
            sectionToUpdate.Order = sectionRequestDto.Order;
            context.SaveChanges();

            return sectionToUpdate;
        }

        public Section Delete(int id)
        {
            var sectionToDelete = GetById(id);
            var deletedSection = context.Sections.Remove(sectionToDelete).Entity;
            this.context.SaveChanges();

            return deletedSection;
        }

        private IQueryable<Section> GetSections()
        {
            return this.context.Sections
                            .Include(section => section.Course);
        }

        private static IQueryable<Section> FilterByTitle(IQueryable<Section> sections, string title)
        {
            return sections.Where(section => section.Title.ToLower().Contains(title));

        }
        private static IQueryable<Section> FilterByOrderNumber(IQueryable<Section> sections, string orderNumber)
        {
            return sections.Where(section => section.Order.ToString().Contains(orderNumber));
        }

        private static IQueryable<Section> SortBy(IQueryable<Section> sections, string sortCriteria)
        {
            return sortCriteria switch
            {
                "title" => sections.OrderBy(section => section.Title),
                "ordernumber" => sections.OrderBy(section => section.Order),
                _ => sections,
            };
        }

        private static IQueryable<Section> OrderBy(IQueryable<Section> sections, string orderBy)
        {
            return (orderBy == "desc") ? sections.Reverse() : sections;
        }
    }
}
