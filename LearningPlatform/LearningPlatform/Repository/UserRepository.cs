﻿using System;
using System.Collections.Generic;
using System.Linq;

using LearningPlatform.Common.DTOs.Requests;
using LearningPlatform.Common.QuerryParameters;
using LearningPlatform.Exceptions;
using LearningPlatform.Helpers;
using LearningPlatform.Identity.Authentication;
using LearningPlatform.Models;
using LearningPlatform.Repository.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace LearningPlatform.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext context;

        public UserRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public User Create(User user)
        {
            if (UserExists(user.Email))
            {
                var message = string.Format(ExceptionMessages.UserExistExceptionMessage);
                throw new UserExistException(message);
            }

            this.context.Users.Add(user);
            this.context.SaveChanges();

            return user;
        }

        public User GetById(int id)
        {
            var user = context.Users.FirstOrDefault(u => u.Id == id);

            var exceptionMessage = string.Format(ExceptionMessages.EntityNotFoundExceptionMessage, id);
            return user ?? throw new EntityNotFoundException(exceptionMessage);
        }
        public IQueryable<User> Get(UserQueryParameters filterParameters)
        {
            string email = !string.IsNullOrEmpty(filterParameters.Email) ? filterParameters.Email.ToLowerInvariant() : string.Empty;
            string firstName = !string.IsNullOrEmpty(filterParameters.FirstName) ? filterParameters.FirstName.ToLowerInvariant() : string.Empty;
            string lastName = !string.IsNullOrEmpty(filterParameters.LastName) ? filterParameters.LastName.ToLowerInvariant() : string.Empty;
            string sortCriteria = !string.IsNullOrEmpty(filterParameters.SortBy) ? filterParameters.SortBy.ToLowerInvariant() : string.Empty;
            string orderBy = !string.IsNullOrEmpty(filterParameters.OrderBy) ? filterParameters.OrderBy.ToLowerInvariant() : string.Empty;

            IQueryable<User> users = context.Users
                                           .Include(user => user.Courses);

            users = FilterByEmail(users, email);
            users = FilterByFirstName(users, firstName);
            users = FilterByLastName(users, lastName);
            users = SortBy(users, sortCriteria);
            users = OrderBy(users, orderBy);

            return users;
        }

        public List<User> GetAllStudents()
        {
            var students = context.Users.Where(u => u.Role == Models.Enums.Role.Student);

            return students.ToList();
        }

        public User Update(int id, UserRequestDto dto)
        {
            var userToUpdate = GetById(id);
            userToUpdate.FirstName = dto.FirstName;
            userToUpdate.LastName = dto.LastName;
            userToUpdate.Password = dto.Password;

            this.context.SaveChanges();

            return userToUpdate;
        }

        public User Delete(int id)
        {
            var userToDelete = GetById(id);

            this.context.Remove(userToDelete);
            this.context.SaveChanges();

            return userToDelete;
        }
        private static IQueryable<User> FilterByEmail(IQueryable<User> result, string email)
        {
            return result.Where(r => r.Email.Contains(email));
        }

        private static IQueryable<User> FilterByFirstName(IQueryable<User> result, string firstName)
        {
            return result.Where(r => r.FirstName.Contains(firstName));
        }

        private static IQueryable<User> FilterByLastName(IQueryable<User> result, string lastName)
        {
            return result.Where(r => r.LastName.Contains(lastName));
        }

        private static IQueryable<User> SortBy(IQueryable<User> result, string sortCriteria)
        {
            return sortCriteria switch
            {
                "email" => result.OrderBy(user => user.Email),
                "firstname" => result.OrderBy(user => user.FirstName),
                "lastname" => result.OrderBy(user => user.LastName),
                _ => result,
            };
        }

        private static IQueryable<User> OrderBy(IQueryable<User> result, string sortOrder)
        {
            return (sortOrder == "desc") ? result.Reverse() : result;
        }

        public bool UserExists(string email)
        {
            return this.context.Users.Any(u => u.Email == email);
        }

        public bool ValidateCredentials(UserCred userCred)
        {
            return this.context.Users.Any(u => u.Email == userCred.Email && u.Password == userCred.Password);
        }

        public User GetByEmail(string email)
        {
            var user = this.context.Users
                .Include(u => u.Courses)
                .FirstOrDefault(u => u.Email == email);

            var exceptionMessage = string.Format(ExceptionMessages.EntityNotFoundExceptionMessageEmail, email);
            return user ?? throw new EntityNotFoundException(exceptionMessage);
        }
    }
}
