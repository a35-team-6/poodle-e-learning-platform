﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearningPlatform.Models
{
    public class Section
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public int Order { get; set; }

        public DateTime CreationTime { get; set; } = DateTime.Now;

        public int CourseId { get; set; }

        public Course Course { get; set; }
    }
}
