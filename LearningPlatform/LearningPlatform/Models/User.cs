﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using LearningPlatform.Constants;
using LearningPlatform.Models.Enums;

namespace LearningPlatform.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Email address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [EmailAddress(ErrorMessage = AttributeMessages.InvalidEmail)]
        public string Email { get; set; }

        [Display(Name = "First name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(32, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = AttributeMessages.RequiredField)]
        [StringLength(16, MinimumLength = 4, ErrorMessage = AttributeMessages.LengthRange)]
        public string Password { get; set; }

        public string Info { get; set; }// For teachers only.

        public Role Role { get; set; } = Role.Student;

        public ProfilePhoto Photo { get; set; }

        public IList<Course> Courses { get; set; } = new List<Course>();

        [NotMapped]
        public bool IsChecked { get; set; }
    }
}
