﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using LearningPlatform.Models.Enums;

namespace LearningPlatform.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }// Unique

        [Required]
        public string Description { get; set; }

        public CourseType CourseType { get; set; }// Enum

        public IList<User> Students { get; set; } = new List<User>();

        public IList<Section> Sections { get; set; } = new List<Section>();
    }
}
