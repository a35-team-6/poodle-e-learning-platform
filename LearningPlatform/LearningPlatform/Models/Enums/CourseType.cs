﻿namespace LearningPlatform.Models.Enums
{
    public enum CourseType
    {
        Public = 1,
        Private = 2,
    }
}
