﻿namespace LearningPlatform.Models.Enums
{
    public enum Role
    {
        Teacher = 1,
        Student = 2
    }
}
