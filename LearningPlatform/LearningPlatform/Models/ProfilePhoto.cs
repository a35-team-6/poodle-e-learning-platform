﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

using Microsoft.AspNetCore.Http;

namespace LearningPlatform.Models
{
    [NotMapped]
    public class ProfilePhoto
    {
        [ForeignKey("User")]
        public int ProfilePhotoId { get; set; }

        public string Name { get; set; }

        public User User { get; set; }

        [NotMapped]
        [DisplayName("Upload File")]
        public IFormFile PhotoFile { get; set; }
    }
}
