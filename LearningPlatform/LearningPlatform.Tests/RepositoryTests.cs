using AutoMapper;
using LearningPlatform.Common.DTOs;
using LearningPlatform.Common.DTOs.Responses;
using LearningPlatform.Controllers.Api;
using LearningPlatform.Data;
using LearningPlatform.Models;
using LearningPlatform.Repository;
using LearningPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LearningPlatform.Tests
{
    [TestClass]
    public class RepositoryTests
    {
        private DbContextOptions<ApplicationContext> options;
        private readonly Mapper mapper;

        public RepositoryTests()
        {

        }

        public RepositoryTests(Mapper mapper)
        {
            this.mapper = mapper;
        }

        [TestInitialize]
        public void Initialize()
        {
            string databaseName = nameof(TestContext.TestName);
            this.options = new DbContextOptionsBuilder<ApplicationContext>().UseInMemoryDatabase(databaseName).Options;
            var context = new ApplicationContext(this.options);

            context.Users.AddRange(ModelBuilderExtensions.GetUsers());
            context.Courses.AddRange(ModelBuilderExtensions.GetCourses());
            context.Sections.AddRange(ModelBuilderExtensions.GetSections());

            context.SaveChanges();
        }

        [TestCleanup]
        public void Cleanup()
        {
            string databaseName = nameof(TestContext.TestName);
            this.options = new DbContextOptionsBuilder<ApplicationContext>().UseInMemoryDatabase(databaseName).Options;
            var context = new ApplicationContext(this.options);
            context.Database.EnsureDeleted();
        }

        [TestMethod]
        public void GetUserById_Should()
        {
            var expectedUser = new User()
            {
                Id = 1,
                Email = "snikolov@telerik.com",
                FirstName = "Stefan",
                LastName = "Nikolov",
                Password = "123123",
                Role = Models.Enums.Role.Teacher
            };

            string databaseName = nameof(GetUserById_Should);
            this.options = new DbContextOptionsBuilder<ApplicationContext>().UseInMemoryDatabase(databaseName).Options;
            var context = new ApplicationContext(this.options);

            var sut = new UserRepository(context);
            var actualUser = sut.GetById(1);

            Assert.AreEqual(expectedUser.Id, actualUser.Id);
            Assert.AreEqual(expectedUser.Email, actualUser.Email);
            Assert.AreEqual(expectedUser.FirstName, actualUser.FirstName);
            Assert.AreEqual(expectedUser.LastName, actualUser.LastName);
            Assert.AreEqual(expectedUser.Password, actualUser.Password);
            Assert.AreEqual(expectedUser.Role, actualUser.Role);
        }

        [TestMethod]
        public void UserController_Should()
        {
            var user = new User()
            {
                Id = 1,
                Email = "snikolov@telerik.com",
                FirstName = "Stefan",
                LastName = "Nikolov",
                Password = "123123",
                Role = Models.Enums.Role.Teacher
            };

            var expectedUser = this.mapper.Map<UserResponseDto>(user);

            var userService = new Mock<IUserService>();
            userService.Setup(service => service.GetById(1))
                .Returns(expectedUser);

            //var authMock = new Mock<AuthorizationHelper>();

            var controller = new UserApiController(userService.Object);
            var actionResult = controller.GetById(1) as ObjectResult;

            var statusCode = actionResult.StatusCode.Value;
            Assert.AreEqual(200, statusCode);

            var actualUser = actionResult.Value as User;
            Assert.AreEqual(user.Email, actualUser.Email);
        }

        [TestMethod]
        public void TestMethod3()
        {
        }
    }
}
