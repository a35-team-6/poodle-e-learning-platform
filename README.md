# eLEARNING PLATFORM
_POODLE e-LEARNING PLATFORM is a learning platform designed to provide educators and learners with a single robust, secure and integrated system to provide online programming learning content._ :computer:
## Description
POODLE e-LEARNING PLATFORM  is a project written in ASP.NET. It Includes a public API and a MVC.
The site features everything you can expect from a online courses platform like creating public and private courses with different sections, enrollmen and unenrollmen of students, etc. The contet of the site is separated on three layers accessibility :closed_lock_with_key: - for visitors, authorized users and teachers. For more information on functionality, down in the document.

## Visuals

### Home Page

![N|Solid](https://i.postimg.cc/s2cnW954/home.jpg)


### Login Page

![N|Solid](https://i.postimg.cc/cL32vnDF/login.jpg)

### Courses Page

![N|Solid](https://i.postimg.cc/j594MtbS/courses2.jpg)

### Single Course Page

![N|Solid](https://i.postimg.cc/x19Mmr5B/Course-Page.jpg)

### Enroll/Unenroll Students Page

![N|Solid](https://i.postimg.cc/85vmHR6Y/Enroll-Students-Page.jpg)


### User Profile Page

![N|Solid](https://i.postimg.cc/CxpJ66N4/main-privet-page.jpg)


### About Page

![N|Solid](https://i.postimg.cc/7L925XRM/about1.jpg)

---

## Features - MVC

> ### Landing page (visible to everyone)
```sh
> The landing page displays only free public courses and you can see a brief description, when you click on them. 
```

> ### About page (visible to everyone)
```sh
> Provides info about the platform and teachers.
```

> ### Courses page (visible only if you are logged-in)
```sh
> Displays all courses in the platform and the user can click to public ones, which will enroll him/her automatically. 
> If the user is not authenticated error will appear, that he/she needs to login first.
```
> **In** order to use the features of the platform, you need an account. 

> ### Login and Register buttons (visible to everyone)
```sh
> Provide options for authorization which includes access to all features, suitable for students, in the forum. Implemented witch JWT Token.
```

> ### Our teachers page (visible to everyone)

```sh
Brief info about the people who runs this place - techers.
```

> ### Drop down menu (visible to everyone, usable only for teachers)
```sh
> Provides additianal options, such as going to all students page for teachers and link to personal profile page for all logged users.

```
> ### Contact us sections (visible to everyone on the footer of the page)
```sh
> Provides an option for sending emails to the development team.
```

> ### Swagger button (visible to everyone on the footer of the page)
```sh
> Provides Swagger API visualisation.
```

## Features - API

Furthermore project supprots Swagger-UI that will visualize APIs documentation and allow you to interact with API resources. 

You can find API yaml file here: [POODLE e-LEARNING API](https://file.io/2x99arJ8ZfjM) :floppy_disk:	

## Technologies used

_POODLE e-LEARNING_ uses a number of open source projects to work properly:
 - ASP.NET Core 5.0
 - ASP.NET Core 5.0 Web API
 - ASP.NET MVC
 - Microsoft Entity Framework Core
 - MSSQL
 - JWT Token
 - AutoMapper
 - Swagger
 - Html 5
 - CSS 3


## Installation :electric_plug:
```sh
> Download the app from the repository.
> Add the connection string to your database in the "DefaultConnection" (appsettings.json file).
> Run the application. The database will be created automatically.
```


## Authors 
Contact us for further information

| Contacts | Emails |
| ------ | ------ |
| Stefan Nikolov | snikolovdev@outlook.com |
| Stanislav Konstantinov | stanislav_ik@abv.bg |



